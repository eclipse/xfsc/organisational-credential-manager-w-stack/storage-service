package handlers

import (
	"errors"

	"github.com/gin-gonic/gin"
	"gitlab.eclipse.org/eclipse/xfsc/organisational-credential-manager-w-stack/storage-service/internal/common"
)

func ErrorResponse(c *gin.Context, err string, exception error) error {
	env := common.GetEnvironment()
	log := env.GetLogger()
	log.Error(nil, err)
	if exception != nil {
		log.Error(exception, "Detailed Error: ")
	}
	c.JSON(400, gin.H{
		"message": err,
	})
	return errors.New(err)
}

func InternalErrorResponse(c *gin.Context, err string, exception error) error {
	env := common.GetEnvironment()
	log := env.GetLogger()
	log.Error(nil, err)
	if exception != nil {
		log.Error(exception, "Detailed Error: ")
	}

	c.JSON(500, gin.H{
		"message": err,
	})
	return errors.New(err)
}
