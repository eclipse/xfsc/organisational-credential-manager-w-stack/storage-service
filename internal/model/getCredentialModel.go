package model

import "gitlab.eclipse.org/eclipse/xfsc/libraries/ssi/oid4vip/model/presentation"

type GetCredentialModel struct {
	Credentials map[string]interface{}      `json:"credentials,omitempty"`
	Receipt     string                      `json:"receipt,omitempty"`
	Groups      []presentation.FilterResult `json:"groups,omitempty"`
}
