package api

import (
	"github.com/gin-gonic/gin"
	"gitlab.eclipse.org/eclipse/xfsc/organisational-credential-manager-w-stack/storage-service/internal/common"
	handlers "gitlab.eclipse.org/eclipse/xfsc/organisational-credential-manager-w-stack/storage-service/internal/handlers/credentials"
)

func AddCredentialRoutes(g *gin.RouterGroup, env *common.Environment) {
	g.PUT("/:id", func(c *gin.Context) {
		handlers.AddCredential(c, env)
	})

	g.DELETE("/:id", func(c *gin.Context) {
		handlers.Remove(c, env, false)
	})

	if env.GetContentType() == common.EncryptedContentType {
		g.GET("", func(c *gin.Context) {
			handlers.Get(c, env, false)
		})
	}

	if env.GetContentType() == common.NormalContentType {
		handlers.GetCredentials(g, env)

		g.POST("/chain", func(c *gin.Context) {
			handlers.Chain(c, env)
		})
	}
}
