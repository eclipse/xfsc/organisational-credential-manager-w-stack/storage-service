package api

import (
	"gitlab.eclipse.org/eclipse/xfsc/organisational-credential-manager-w-stack/storage-service/internal/common"
	handlers "gitlab.eclipse.org/eclipse/xfsc/organisational-credential-manager-w-stack/storage-service/internal/handlers/health"

	"github.com/gin-gonic/gin"
)

func AddHealth(g *gin.RouterGroup, env *common.Environment) {
	g.GET("/health", func(c *gin.Context) {
		handlers.AddHealth(c, env)
	})
}
