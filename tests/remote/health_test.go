package tests

import (
	"net/http"
	"net/http/httptest"
	"strconv"
	"testing"

	"gitlab.eclipse.org/eclipse/xfsc/organisational-credential-manager-w-stack/storage-service/internal/api"
	"gitlab.eclipse.org/eclipse/xfsc/organisational-credential-manager-w-stack/storage-service/internal/common"

	"github.com/gin-gonic/gin"
)

var healthEnv *common.Environment

func TestHealth(t *testing.T) {
	healthEnv = new(common.Environment)
	engine := gin.Default()
	group := engine.Group("/test")

	api.AddHealth(group, healthEnv)

	recorder := httptest.NewRecorder()
	request, err := http.NewRequest("GET", "/test/health", nil)

	if err != nil {
		t.Error("Request Building failed.")
	}

	engine.ServeHTTP(recorder, request)

	if recorder.Code != 400 {
		t.Error("Health Request shows wrong statuscode. Code was: " + strconv.Itoa(recorder.Code))
	}
}
